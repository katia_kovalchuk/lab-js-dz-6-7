import { Component, OnInit } from '@angular/core';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { FamilyService } from '../../services/family.service';

@Component({
  selector: 'lab-js-add-family',
  templateUrl: './add-family.component.html',
  styleUrls: ['./add-family.component.scss']
})
export class AddFamilyComponent implements OnInit {
  public familyForm: FormGroup;
  public get children() {
    return this.familyForm.get('children') as FormArray;
  }
  public constructor(
    private readonly familyService: FamilyService
  ) {

  }
  public ngOnInit(): void {
    this.createForm();
  }

  private createForm(){
    this.familyForm = new FormGroup({
      name: new FormControl(null, [Validators.required]),
      father: this.createFormGroup(),
      mother: this.createFormGroup(),
      children: new FormArray([this.createFormGroup()]),
    })
  }

  private createFormGroup(){
    return new FormGroup({
      name: new FormControl(null, [Validators.required]),
      age: new FormControl(null, [Validators.required])
    })
  }

  public addChild() {
    const child = this.createFormGroup();
    (this.familyForm.get('children') as FormArray).push(child);
  }
  public removeChild(index: number) {
    (this.familyForm.get('children') as FormArray).removeAt(index);
  }
  public submit() {
    this.familyService.addFamily$(this.familyForm.value).subscribe();
  }
}
